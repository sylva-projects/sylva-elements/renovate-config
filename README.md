# renovate-config

Project to store common renovate configurations

default.json used as default preset in all repos

docker.json inherit from default.json and add some dedicated rules for repos using dockerfile

go.json inherit from docker.json and add specific go rules


Syntax example to use a preset:

```
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>sylva-projects/sylva-elements/renovate-config:docker"
  ]
}
``` 

